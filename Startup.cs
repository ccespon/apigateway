using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System;
using System.Text;

namespace Api.Gateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string textKey = Configuration.GetSection("JWTSettings").GetSection("Secret").Value;

            string validIssuer = Configuration.GetSection("JWTSettings").GetSection("Iss").Value;
            string validAudience = Configuration.GetSection("JWTSettings").GetSection("Aud").Value;
            var validateIssuer = Configuration.GetSection("JWTSettings").GetSection("ValidateIssuer").Value;
            var validateAudience = Configuration.GetSection("JWTSettings").GetSection("ValidateAudience").Value;
            var validateLifetime = Configuration.GetSection("JWTSettings").GetSection("ValidateLifetime").Value;
            var requireExpirationTime = Configuration.GetSection("JWTSettings").GetSection("RequireExpirationTime").Value;
            var authenticationProviderKey = Configuration.GetSection("JWTSettings").GetSection("AuthenticationProviderKey").Value;
            

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(textKey));

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = false,
                ValidIssuer = validIssuer,
                ValidateAudience = false,
                ValidAudience = validAudience,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true,
            };


            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
              .AddJwtBearer(authenticationProviderKey, x =>
              {
                  x.RequireHttpsMetadata = false;
                  x.TokenValidationParameters = tokenValidationParameters;
              });

            services.AddOcelot();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();




            var configuration = new OcelotPipelineConfiguration
            {
                PreQueryStringBuilderMiddleware = async (ctx, next) =>
                {
                    await next.Invoke();
                }
            };

            app.UseOcelot();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
